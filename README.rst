============================================================
Intel Graphics for Linux - drm/i915 Kernel Driver Repository
============================================================

The drm/i915 kernel driver repository. Visit the branch guide in the repository
description for more info on where to go next.

Please see the `developer documentation`_ and `bug filing guide`_.

.. _developer documentation: https://drm.pages.freedesktop.org/intel-docs/

.. _bug filing guide: https://drm.pages.freedesktop.org/intel-docs/how-to-file-i915-bugs.html
